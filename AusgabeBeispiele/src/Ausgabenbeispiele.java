
public class Ausgabenbeispiele {

	public static void main(String[] args) {
		
		// Kommazahl
	
		System.out.printf(" zahl: *%-20.2f* %n", 12345.6789);
		
		// Ganzezahl
		
		System.out.printf(" zahl: *%-20d* %n", 123456789);
		
		// Zeichenkette (String)
		
		System.out.printf(" Wort: *%-20.4s* ", "abcdef");

	}

}
